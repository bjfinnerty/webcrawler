from django.http import HttpResponse, HttpResponseRedirect
from celerytasks.Crawler.tasks import Crawler

def index(request):

    # class Project(object):
    #
    #     def __init__(self, homepage):
    #         self.name = 'sample'
    #         self.parse_lib = 'html5lib'
    #         self.homepage = homepage
    #
    # project = Project(request.GET.get('homepage'))
    # print project.homepage

    project = {
        'name':'sample',
        'parse_lib':'html5lib',
        'homepage':request.GET.get('homepage')
    }

    crawler = Crawler(project, limit=20)
    crawler.crawl_and_scrape(project.get('homepage'))

    return HttpResponse('Processing your site')
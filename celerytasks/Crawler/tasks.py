import requests, re, redis
import logging
from celery.contrib.methods import task_method
from celerytasks.celeryapp import app
from celery import group
from eventlet import Timeout
from bs4 import NavigableString as ns, BeautifulSoup as bs
from utils import *

# CONSTANTS
BROWSER_HEADERS = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:18.0) Gecko/20100101 Firefox/18.0'}
CRAWLER_MAX = 5000
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

logger = logging.getLogger(__name__)


r_conn = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=11)

class Crawler(object):

    def __init__(self, project, limit=CRAWLER_MAX, avoidable=None, cookies=None, tidy_redis=True):

        self.project = project
        self.name = project.get('name')
        self.project_parser = getattr(self.project, "parse_lib", "lxml")
        self.limit = limit
        self.avoidable = avoidable or []
        self.base_url = project.get('homepage')
        self.cookies = cookies or {}
        self.translatables_key = ":".join([self.name, 'elements'])
        self.links_key = ":".join([self.name, 'links'])
        self.pages_key = ":".join([self.name, 'pages'])
        self.callback_key = ":".join([self.name, 'callback_called'])
        self.crawled = {self.base_url:"found"}
        self.already_saved = {}
        r_conn.set(self.callback_key, 0)

        if tidy_redis:
            self.tidy_redis()


    def get_page(self, url):
        with Timeout(5, False):
            try:
                print 'crawling: ' + url
                response = requests.get(url, headers=BROWSER_HEADERS, allow_redirects=True, cookies=self.cookies)
            except Exception as e:
                logger.exception(e)
                r_conn.hset(self.links_key, url, 'bad')
                return False
            else:
                if is_domain(response.url, self.base_url) and valid_html_page(response):
                    r_conn.hmset(self.links_key, {response.url: 'scraped', url:'scraped'})
                    r_conn.sadd(self.pages_key, response.url)
                    try:
                        response.soup = bs_response_body(response, project_parser=self.project_parser)
                    except :
                        logger.exception('problem parsing page {0}'.format(response.url))
                        r_conn.hset(self.links_key, response.url, 'bad')
                else:
                    r_conn.hset(self.links_key, url, 'bad')

                return response



    def process_anchors(self, links):
        valid_urls = set()
        for link in links:
            if filter_links(link, self.base_url, avoidable=self.avoidable):
                new_link = fix_link(self.base_url, link)
                if new_link not in r_conn.hkeys(self.links_key) and len(r_conn.hkeys(self.links_key)) < self.limit:
                    r_conn.hset(self.links_key, new_link, 'found')
                    valid_urls.add(new_link)

        return valid_urls


    @app.task(filter=task_method, ignore_result=False)
    def crawl_and_scrape(self, url):

        response = self.get_page(url)
        try:
            soup = response.soup or bs()
            url = response.url
        except AttributeError as e:
            self.check_if_done()
            return False

        elements = parse_page_html(soup)

        links = elements.get('anchor')
        valid_urls = self.process_anchors(links)
        self.save_els_to_memory(elements, url)

        if len(valid_urls) and len(r_conn.smembers(self.pages_key)) <= self.limit:
            subtasks = group(self.crawl_and_scrape.s(valid_url) for valid_url in valid_urls)
            group_task = subtasks()

        self.check_if_done()
        return url


    def save_els_to_memory(self, elements, url):

        for str_type, els in elements.items():
            for el in els:
                el = as_raw(el)
                mod_key = ':'.join([self.name,'translatable', str_type, el])
                r_conn.sadd(mod_key, url)
                r_conn.sadd(self.translatables_key, mod_key)

        return None

    def check_if_done(self):

        page_statuses = r_conn.hvals(self.links_key)
        if not int(r_conn.get(self.callback_key)):
            if 'found' not in page_statuses:
                r_conn.set(self.callback_key,1)
                print "No more pages to find. Callback time"
                return self.finished_callback()
            else:
                return False
        else:
            return False

    @run_once
    def finished_callback(self):

        page_keys = r_conn.smembers(self.pages_key)
        translatable_keys = r_conn.smembers(self.translatables_key)

        # Do whatever is needed here with the content stored in redis then tidy up
        print page_keys
        # self.tidy_redis()

        return self

    def tidy_redis(self):
        # clear redis variables
        pipe = r_conn.pipeline()
        r_conn.delete(self.links_key)
        r_conn.delete(self.pages_key)
        #r_conn.delete(self.callback_key)
        all_els = r_conn.smembers(self.translatables_key)
        for el in all_els:
            pipe.delete(el)
        pipe.delete(self.translatables_key)
        pipe.execute()

        return 'redis cleared out'




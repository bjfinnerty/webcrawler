from urllib import urlencode
from urlparse import urlparse, parse_qs, urlsplit, urlunsplit, urljoin
from bs4 import NavigableString as ns4, BeautifulSoup as bs4
import logging, re, htmlentitydefs
logger = logging.getLogger(__name__)

# Utility functions for crawler

def url_filter(soup, base_url, avoidable=None):
    url_list = [fix_link(base_url, link) for link in soup.find_all("a", href=True) if filter_links(link, base_url, avoidable)]
    return url_list


def valid_html_page(resp):

    if resp.status_code != 200:
        return False

    if resp.headers.get('X-Reverbeo-Translate', False) == 'No':
        print 'REVERBEO IGNORE: ' + resp.url
        return False

    if resp.text.find("<!DOCTYPE") == -1 and resp.text.find("<!doctype") == -1:
        print resp.url + ' has no <!DOCTYPE element'
        return False

    bad_mimes = ['application/pdf', 'text/xml', 'image/jpeg', "application/rss+xml", "'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'"]
    for mime in bad_mimes:
        if mime in resp.headers.get('content-type'):
            return False

    if resp.headers.get('content-disposition') and 'attachment' in resp.headers.get('content-disposition'):
        return False

    return True


def fix_link(url, link):

    USELESS_QPS = ['phpsessid', 'referuri']
    link_url = urlsplit(link.get('href'), allow_fragments=True)
    link_url_list = list(link_url)

    if link_url.query:
        link_url_list[3] = urlencode(dict((k,v) for k, vlist in parse_qs(link_url.query).iteritems() for v in vlist if k.lower() not in USELESS_QPS))
    else:
        link_url_list[3] = link_url.query

    path = re.sub(r'index[.](\w+)$', "", link_url.path)
    if not path.endswith("/") and path.rfind('.') < path.rfind('/'):
        path += "/"
    link_url_list[2] = path

    link_url_list[4] = ''
    new_link = urljoin(url.strip(), urlunsplit(link_url_list).strip())

    try:
        return new_link
    except ValueError as e:
        logger.exception(e)
        return link_url


def filter_links(link, root_url, avoidable=None):

    href = link.get('href').strip().split('?')[0]
    ext = file_extension(href)
    if filter(href.startswith,['#','mailto:','javascript:','tel:']):
        return False
    if any(patt in href for patt in avoidable):
        return False
    elif not is_domain(href, root_url):
        return False
    elif ext.lower() in ["jpg", "gif", "zip", "pdf",
                         "png", "doc", "docx", "swf",
                         "ppt", "pptx", "tar", "tif",
                         "tiff", "xlsx", "eps", "dwg",
                         "jar", "mp3", "mp4","xls",
                         "wmv", "dxf","dmg", "exe"]:
        return False
    else:
        return True


def is_domain(url, root):
    url_domain = urlparse(url).netloc.replace("www.", "")

    if not url_domain:
        # relative link
        return True
    else:
        root_domain = urlparse(root.replace("www.", "")).netloc
        if root_domain == url_domain:
            return True
        else:
            return False


def file_extension(value):
    splitter = "."
    if splitter in value:
        val = value.split(splitter)[-1]
    else:
        val = ""
    return val.lower()

def parse_page_html(soup, disallowed=None, page_nodes=None):

    # @param : soup (BeautifulSoup html page) - the page we are parsing
    # @param : disallowed (list:optional) - list of tag names that we should not extract content from
    # @param : page_nodes (json object:optional) - can be passed in when we need to call the function recursively

    # Main function for parsing a webpage. Called by the proxy and the crawler
    # Step down through the dom tree pulling out elements based on their properties
    # if a node has text and tags, save it as a fragment

    # returns a json object of the Beautiful Soup elements we need to translate / save to the database

    page_nodes = page_nodes or {'string':[], 'meta':[], 'form':[], 'image':[], 'anchor':[], 'fragment':[]}
    disallowed = disallowed or ['style', 'script', '[document]', 'head', 'description']

    for node in soup.descendants:

        if type(node) == ns4 and len(node.strip()) and node.parent.name not in disallowed:
            if len(node.fetchNextSiblings()+node.fetchPreviousSiblings()):
                if node.parent not in page_nodes['fragment'] and node.parent not in page_nodes['anchor']:
                    page_nodes['fragment'].append(node.parent)

            page_nodes['string'].append(node)

        elif node.parent.name == 'description':
            node_soup = bs4(node)
            new_page_nodes = parse_page_html(node_soup, disallowed=['style', 'script', '[document]', 'head'], page_nodes=page_nodes)

        elif node.name:
            if node.name == 'meta' and node.get('content') and node.get('name') not in ['viewport, google-site-verification', 'author', 'robots', 'msvalidate.01', 'alexaVerifyID' ] and not node.get('http-equiv'):
                page_nodes['meta'].append(node)

            elif node.name == 'img' and node.get('src'):
                page_nodes['image'].append(node)

            elif node.name == 'a' and node.get('href'):
                page_nodes['anchor'].append(node)

            elif node.name == 'input' and node.get('type') != 'hidden' and (node.get('value') or node.get('placeholder')):
                page_nodes['form'].append(node)

    return page_nodes


def run_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
        else:
            print "Already Run Once"
    wrapper.has_run = False
    return wrapper

def as_raw(el):
    return unescape(re.sub( '\s+', ' ', unicode(el)).strip())

def bs_response_body(response, project_parser='lxml'):

    soup = bs4(response.text, project_parser)
    try:
        page_charset = soup.find('meta', attrs={'charset':True}).get('charset')
    except AttributeError as e:
        return soup
    else:
        if page_charset.lower() != response.encoding.lower():
            response.encoding = page_charset
            soup = bs4(response.text, project_parser)

    return soup

def findnth(haystack, needle, n):
    parts = haystack.split(needle, n + 1)
    if len(parts) <= n + 1:
        return -1
    return len(haystack) - len(parts[-1]) - len(needle)


def unescape(text):
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return unichr(int(text[3:-1], 16))
                else:
                    return unichr(int(text[2:-1]))
            except ValueError:
                pass
        else:
            # named entity
            try:
                text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
            except KeyError:
                pass
        return text # leave as is
    return re.sub("&#?\w+;", fixup, text)
## Broker settings.
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
#BROKER_URL = 'redis://localhost:6379/3'

# List of modules to import when celery starts.
#CELERY_IMPORTS = ('myapp.tasks', )

## Using the database to store task state and results.
CELERY_RESULT_BACKEND = 'redis://localhost/10'

#CELERY_ANNOTATIONS = {'tasks.add': {'rate_limit': '10/s'}}
CELERY_ALWAYS_EAGER = False

from celery.schedules import crontab, timedelta

CELERYBEAT_SCHEDULE = {
    # Executes once a day at midday
    'run-once-a-day': {
        'task': 'celerytasks.Notifications.tasks.updated_content',
        'schedule': crontab(minute=0, hour=12),
        'args': [['abbeylee','bullbuster','merrilbrink','powerling','floatinghospital']],
        },

    # Executes every hour
    'run-every-hour': {
        'task': 'celerytasks.Notifications.tasks.add_queued_strings',
        'schedule': crontab(minute=0),
        'args': [],
        },

    # Executes every hour
    #'test-every-5-seconds': {
    #    'task': 'celerytasks.tasks.add',
    #    'schedule': timedelta(seconds=5),
    #    'args': [5,6],
    #    }
}
from Crawler.tasks import Crawler
from celeryapp import app


# test tasks

@app.task(ignore_result=False)
def add(x, y):
    return x + y

@app.task
def tsum(numbers):
    print numbers
    return sum(numbers)


from celery import group, chord
from Crawler.tasks import Crawler, is_domain, findnth, parse_page_html
from celerytasks.tasks import add, tsum
from django.test import TestCase
from manager.CouchDBModels.UserModels import User
import time
import redis, pickle
from bs4 import BeautifulSoup as bs
from manager.settings import REDIS_HOST, REDIS_PORT
import requests

class SimpleTest(TestCase):

    def setUp(self):
        self.result_backend = redis.StrictRedis(host='127.0.0.1', port=6379, db=10)

    def test_basic_addition(self):

        '''
            just checking that the task queue is functioning
        '''

        array =  range(0,10)
        callback = tsum.s()
        header = group(add.s(i, i) for i in array)
        result = chord(header)(callback)
        time.sleep(5)
        self.assertTrue(result.successful(), msg=result.status)
        self.assertEqual(result.get(), 90)
        self.result_key = 'celery-task-meta-'+result.id
        result_dict = pickle.loads(self.result_backend.get(self.result_key))
        print result_dict
        self.assertEqual(result_dict.get('result'), result.get())
        self.assertEqual(result_dict.get('status'), result.status)
        self.result_backend.delete(self.result_key)



class CrawlerTest(TestCase):


    def setUp(self):
        self.user = User(username='unittest@reverbeo.com', password='unittest')
        self.project = self.user.get_current_project()
        self.r_conn = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=11)


    def test_crawler(self):

        crawler = Crawler(self.project, limit=5)
        r_conn = self.r_conn
        self.assertEqual(crawler.project, self.project)
        self.assertEqual(crawler.limit, 5)
        self.assertEqual(crawler.base_url, self.project.homepage)
        self.assertEqual(crawler.links_key, self.project.name+":links")
        self.assertEqual(crawler.pages_key, self.project.name+":pages")
        self.assertEqual(crawler.translatables_key, self.project.name+":elements")
        self.assertEqual(crawler.callback_key, self.project.name+":callback_called")
        self.assertIsInstance(crawler.crawled, dict)
        self.assertIsInstance(crawler.already_saved, dict)

        self.assertEqual(len(r_conn.hgetall(crawler.links_key)), 0)
        self.assertEqual(len(r_conn.smembers(crawler.pages_key)), 0)
        self.assertEqual(int(r_conn.get(crawler.callback_key)), 0)

        url = self.project.homepage
        response = crawler.get_page(url)
        soup = response.soup
        links_processed = r_conn.hgetall(crawler.links_key)
        pages_processed = r_conn.smembers(crawler.pages_key)

        self.assertIsInstance(response, requests.Response)
        self.assertIsInstance(soup, bs)
        self.assertEqual(len(links_processed), 1) # the homepage and the new page
        self.assertEqual(len(pages_processed), 1)
        self.assertEqual(links_processed[url], 'scraped')
        self.assertIn(url, pages_processed)


        elements = parse_page_html(soup)
        valid_urls = crawler.process_anchors(elements.get('anchor'))

        self.assertEqual(len(valid_urls)+1, crawler.limit, msg=[elements.get('anchor'), self.project.name])
        for link in valid_urls:
            self.assertTrue(is_domain(link, self.project.homepage))
            self.assertEqual(r_conn.hget(crawler.links_key, link), 'found')


        crawler.save_els_to_memory(elements, url)
        temp_models = r_conn.smembers(crawler.translatables_key)
        self.assertIsInstance(temp_models,set)
        self.assertGreater(len(temp_models), 0)

        for mod in temp_models:
            temp_mod_pages = r_conn.smembers(mod)
            self.assertIsInstance(temp_mod_pages, set)
            self.assertGreater(len(temp_mod_pages), 0)

            mod_type = mod[findnth(mod,":",1)+1:findnth(mod,":",2)]
            self.assertIn(mod_type, ['string', 'anchor', 'meta', 'fragment', 'image', 'form'])

            mod_val = mod[findnth(mod,":",2)+1:]
            try:
                crawler.get_model(mod_val)
            except AttributeError as e:
                self.assertIsInstance(e, AttributeError)

            new_model = crawler.make_model(self.user, self.project, mod_val, mod_type, list(temp_mod_pages), 'fake_id')
            self.assertIsNotNone(new_model)

            self.assertEqual(list(temp_mod_pages), new_model.pages)

        for mod in temp_models:
            mod_val = mod[findnth(mod,":",2)+1:]
            self.assertIsNotNone(crawler.get_model(mod_val), msg=mod_val)

        self.assertFalse(crawler.check_if_done())
        run_once = crawler.fake_callback()
        run_twice = crawler.fake_callback()

        self.assertEqual(run_once, 'Fake Callback')
        self.assertIsNone(run_twice)

        crawler.tidy_redis()
        self.assertEqual(len(r_conn.smembers(crawler.translatables_key)),0)
        self.assertEqual(len(r_conn.smembers(crawler.pages_key)),0)
        self.assertEqual(len(r_conn.hgetall(crawler.links_key)),0)

# README #

Python / Django App to recursively crawl a website  

### What is this repository for? ###

A python / django web crawler that takes a url and crawls all the urls on that page recursively until it finds no more. The pages are parsed for their content and a map of the urls and the content is saved to redis. It uses celery, and rabbitmq for creating worker tasks and redis to store the parsed data